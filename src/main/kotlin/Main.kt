import javax.swing.text.Style

fun main(args: Array<String>) {
    val number: Int = 2
    val i: Byte = number.toByte()
    val s: String = number.toString()
    val f: Float = number.toFloat()

    val e = 35_000_000
    val a = 3_000
    print("your number is" + number)
    println(i)
    println(s)
    println(f)
    println(e)
    println(a)
    println("number is $number, i is $i, s is $s")
    print("e + a = ${e + a}")

    val x = 10
    var y = 10
    y = 11
    println("x is $x, y is $y")


    // calculate grade
    val c = 78 // valiable grade = 78

    if (a > 80) {
        println("A")
    } else if (c in 70..80) {
        println("B")
    } else if (c in 60..70) {
        println("C")
    } else {
        println("F")
    }


    when(c){
        in 80 .. 100 -> println("A")
        in 70 .. 79 -> println("B")
        in 60 .. 69 -> println("C")

        else -> println("F")
    }

    when {
        c in 80 .. 100 -> println("A")
        c in 70 .. 79 -> println("B")
        c in 60 .. 69 -> println("C")

        else -> println("F")
    }

    val grade = when (c) {
        in 80 .. 100 -> "A"
        in 70 .. 79 -> "B"
        in 60 .. 69 -> "C"
        else -> "F"
    }
    println(grade)
}

